<?php

namespace Tests\Feature;

use App\TrackedTime;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Passport\Passport;
use Tests\TestCase;

class TrackedTimeControllerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function index_returns_all_times_for_an_user()
    {
        $user = factory(User::class)->create();

        $startTime = Carbon::now();
        $endTime = $startTime->addDay();

        factory(TrackedTime::class)->create([
            'user_id' => $user->id,
            'start' => $startTime,
            'end' => $endTime
        ]);
        Passport::actingAs($user);

        $res = $this->get('/api/times');

        $res->assertStatus(200);
        $res->assertJson([
            [
                'id' => 1,
                'user_id' => $user->id,
                'start' => $startTime->timestamp,
                'end' => $endTime->timestamp
            ],
        ]);
    }

    /**
     * @test
     */
    public function index_only_times_that_belong_to_an_user()
    {
        $user = factory(User::class)->create();

        Passport::actingAs($user);


        factory(TrackedTime::class)->create([
            'user_id' => $user->id,
        ]);


        factory(TrackedTime::class)->create([
            'user_id' => 999,
        ]);

        $res = $this->get('/api/times');
        $res->assertStatus(200);

        $json = $res->json();

        $this->assertCount(1, $json);
        $this->assertEquals($user->id, $json[0]['user_id']);
        $this->assertNotEquals(999, $json[0]['user_id']);

    }


    /**
     * @test
     */
    public function show_returns_a_specific_time()
    {
        $user = factory(User::class)->create();

        Passport::actingAs($user);

        factory(TrackedTime::class)->create([
            'user_id' => $user->id,
            'id' => 1
        ]);

        $res = $this->get('/api/times/1');
        $res->assertStatus(200);
        $res->assertJson(
            [
                'id' => 1,
                'user_id' => $user->id,
            ]);
    }

    /**
     * @test
     */
    public function store_creates_a_new_time()
    {
        $user = factory(User::class)->create();

        $start = Carbon::now()->subDay()->timestamp;
        $end = Carbon::now()->timestamp;

        Passport::actingAs($user);
        $res = $this->postJson('/api/times/', [
            'start' => $start,
            'end' => $end
        ]);

        $res->assertStatus(201);

        $res->assertJson([
            'id' => 1,
            'user_id' => $user->id,
            'start' => $start,
            'end' => $end
        ]);
    }

    /** @test */
    public function a_guest_can_not_add_a_new_time()
    {
        $user = factory(User::class)->create();

        $start = Carbon::now()->subDay()->timestamp;
        $end = Carbon::now()->timestamp;
        $res = $this->postJson('/api/times/', [
            'user_id' => 1,
            'start' => $start,
            'end' => $end
        ]);
        $res->assertStatus(401);
    }

    /** @test */
    public function the_start_date_is_required()
    {
        $user = factory(User::class)->create();

        $end = Carbon::now()->timestamp;
        Passport::actingAs($user);

        $res = $this->postJson('/api/times/', [
            'user_id' => 1,
            'end' => $end
        ]);
        $res->assertStatus(422);

        $this->assertValidationError($res, 'start');
    }

    /** @test */
    public function the_end_date_is_required()
    {
        $user = factory(User::class)->create();

        $start = Carbon::now()->subDay()->timestamp;
        Passport::actingAs($user);

        $res = $this->postJson('/api/times/', [
            'user_id' => 1,
            'start' => $start
        ]);
        $res->assertStatus(422);

        $this->assertValidationError($res, 'end');
    }

    /** @test */
    public function a_guest_can_not_view_any_times()
    {
        $user = factory(User::class)->create();

        $startTime = Carbon::now();
        $endTime = $startTime->addDay();

        factory(TrackedTime::class)->create([
            'user_id' => $user->id,
            'start' => $startTime,
            'end' => $endTime
        ]);

        $res = $this->getJson('/api/times');

        $res->assertStatus(401);
    }

    /** @test */
    public function a_guest_can_not_view_a_spcefic_time()
    {
        $user = factory(User::class)->create();

        $startTime = Carbon::now();
        $endTime = $startTime->addDay();

        $time = factory(TrackedTime::class)->create([
            'user_id' => $user->id,
            'start' => $startTime,
            'end' => $endTime
        ]);

        $res = $this->getJson('/api/times/'. $time->id);

        $res->assertStatus(401);
    }


    /** @test */
    public function a_404_is_returned_when_a_user_wants_to_see_other_times()
    {
        $user = factory(User::class)->create();
        $otherUser = factory(User::class)->create();

        Passport::actingAs($user);

        $startTime = Carbon::now();
        $endTime = $startTime->addDay();

        $time = factory(TrackedTime::class)->create([
            'user_id' => $otherUser->id,
            'start' => $startTime,
            'end' => $endTime
        ]);

        $res = $this->getJson('/api/times/' . $time->id);
        $res->assertStatus(404);
    }


}
