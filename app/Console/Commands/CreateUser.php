<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'timey:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create a new user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mail = $this->ask('Whats the mail?');
        $password = bcrypt($this->ask('Say the magic word'));


        $user = new User();
        $user->name = 'Admin';
        $user->email = $mail;
        $user->password = $password;
        $user->save();
    }
}
