<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackedTime extends Model
{

    protected $guarded = [];

    protected $casts = [
        'start' => 'timestamp',
        'end' => 'timestamp'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
