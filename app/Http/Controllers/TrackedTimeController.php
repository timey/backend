<?php

namespace App\Http\Controllers;

use App\TrackedTime;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class TrackedTimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Auth::user()->trackedTimes;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $validData = $this->validate($request, [
            'start' => 'required',
            'end' => 'required'
        ]);

        /** @var User $user */
        $time = $user->trackedTimes()->create([
            'start' => Carbon::createFromTimestamp($validData['start']),
            'end' => Carbon::createFromTimestamp($validData['end'])
        ]);

        return response()->json($time, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param TrackedTime $trackedTime
     */
    public function show($id)
    {
        $trackedTime = Auth::user()->trackedTimes()->findOrFail($id);
        return response()->json($trackedTime);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TrackedTime $trackedTime
     * @return \Illuminate\Http\Response
     */
    public function edit(TrackedTime $trackedTime)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\TrackedTime $trackedTime
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TrackedTime $trackedTime)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TrackedTime $trackedTime
     * @return \Illuminate\Http\Response
     */
    public function destroy(TrackedTime $trackedTime)
    {
        //
    }
}
